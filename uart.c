#include "type.h"
#include "platform.h"

// get UART register address
#define UART_R(register) ((volatile uint8_t*)(UART0 + register))

/*
 * UART control registers map. see [1] "PROGRAMMING TABLE"
 * note some are reused by multiple functions
 * 0 (write mode): THR/DLL
 * 1 (write mode): IER/DLM
 */
#define RHR 0	// Receive Holding Register (read mode)
#define THR 0	// Transmit Holding Register (write mode)
#define DLL 0	// LSB of Divisor Latch (write mode)
#define IER 1	// Interrupt Enable Register (write mode)
#define DLM 1	// MSB of Divisor Latch (write mode)
#define FCR 2	// FIFO Control Register (write mode)
#define ISR 2	// Interrupt Status Register (read mode)
#define LCR 3	// Line Control Register
#define MCR 4	// Modem Control Register
#define LSR 5	// Line Status Register
#define MSR 6	// Modem Status Register
#define SPR 7	// ScratchPad Register

#define LSR_RX_READY (1 << 0)
#define LSR_TX_IDLE  (1 << 5)

#define uart_read_r(register) (*(UART_R(register)))
#define uart_write_r(register, v) (*(UART_R(register)) = (v))

void uart_init() {
    uart_write_r(IER, 0x00);

    uint8_t lcr = uart_read_r(LCR);
    uart_write_r(LCR, lcr | (1 << 6));
    uart_write_r(DLL, 0x03);
    uart_write_r(DLM, 0x00);

    lcr = 0;
    uart_write_r(LCR, lcr | (3 << 1));
}

int uart_putc(char ch) {
    while ((uart_read_r(LSR) & LSR_TX_IDLE) == 0);
    return uart_write_r(THR, ch);
}

void uart_puts(char *s){
    while (*s) {
        uart_putc(*s++);
    }
}
