#include "os.h"

#define STACK_SIZE 31

uint8_t task_stack[STACK_SIZE];
struct context ctx_tsk;

// mscratch: 它暂时存放一个字大小的数据
static void w_mscratch(reg_t x)
{
    asm volatile(
        "csrw mscratch, %0" ::"r"(x));
}

void user_task0(void);
void sched_init()
{
    w_mscratch(0);

    ctx_tsk.sp = (reg_t) &task_stack[STACK_SIZE - 1];
    ctx_tsk.ra = (reg_t)user_task0;
}

void schedule()
{
    struct context *next_tsk = &ctx_tsk;
    switch_to(next_tsk);
}

/*
 * a very rough implementaion, just to consume the cpu
 */
void task_delay(volatile int count)
{
    count *= 5000000;
    while (count--)
        ;
}

void user_task0(void)
{
    uart_puts("Task 0: Created!\n");
    int count_down = 10;
    while (count_down)
    {
        uart_puts("Task 0: Running...\n");
        task_delay(10);
        count_down -= 1;
    }
}