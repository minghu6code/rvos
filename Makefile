CROSS_COMPILE = riscv64-unknown-elf-

# fno-builtin: disable builtin funtion optimization
# nostdlib: don't use standard library when linking
# Wall: warning all
# g: with debug info
CFLAGS = -nostdlib -fno-builtin -march=rv32ima -mabi=ilp32 -g -Wall

QEMU = qemu-system-riscv32

# smp: n, cpu number
QFLAGS = -nographic -smp 1 -machine virt -bios none

GDB = ${CROSS_COMPILE}gdb
GCC = ${CROSS_COMPILE}gcc
OBJCOPY = ${CROSS_COMPILE}objcopy
OBJDUMP = ${CROSS_COMPILE}objdump

SRCS_ASM = \
	start.S \
	mem.S \
	entry.S \


SRCS_C = \
	kernel.c \
	uart.c \
	page.c \
	printf.c \
	sched.c \


OBJS = ${SRCS_ASM:.S=.o}
OBJS += ${SRCS_C:.c=.o}

.DEFAULT_GOAL := all

all: os.elf

# ref https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html#Automatic-Variables
os.elf: ${OBJS}
	${GCC} ${CFLAGS} -T os.ld -o os.elf $^
	${OBJCOPY} -O binary os.elf os.bin

%.o: %.c
	${GCC} ${CFLAGS} -c -o $@ $<

%.o: %.S
	${GCC} ${CFLAGS} -c -o $@ $<

run: all
	@ ${QEMU} -M ? | grep virt >/dev/null || exit
	@ echo "Press Ctrl-A and then just X to exit QEMU"
	@ echo "------------------------------------"
	@ ${QEMU} ${QFLAGS} -kernel os.elf

.PHONY: debug
debug: all
	@echo "Press Ctrl-C and then input 'quit' to exit GDB and QEMU"
	@echo "-------------------------------------------------------"
	@${QEMU} ${QFLAGS} -kernel os.elf -s -S &
	@${GDB} os.elf -q -x ./gdbinit

.PHONY: code
code: all
	@${OBJDUMP} -S os.elf | less

.PHONY: clean
clean:
	rm -rf *.o *.bin *.elf

.PHONY: kill-qemu
kill-qemu:
	@pidof qemu-system-riscv32 | xargs kill -s 9
